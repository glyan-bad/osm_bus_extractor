/*
 * Copyright (c) 2020.  Gauthier LYAN
 */

package com.tritcorp.transport.osmBusExtractor

import slick.lifted.ProvenShape
import slick.jdbc.PostgresProfile.api._

class InterPoints(tag: Tag)
  extends Table[(Long, String, Int, String, Int)](tag, "interpoints") {

  def trip_id: Rep[Long] = column[Long]("trip_id")

  def orig: Rep[String] = column[String]("orig")

  def order1: Rep[Int] = column[Int]("order1")

  def dest: Rep[String] = column[String]("dest")

  def order2: Rep[Int] = column[Int]("order2")

  def * : ProvenShape[(Long, String, Int, String, Int)] =
    (trip_id, orig, order1, dest, order2)

}

object InterPoints {

  case class InterPoint(trip_id: Long, orig: String, order1: Int, dest: String, order2: Int)

}