/*
 * Copyright (c) 2020.  Gauthier LYAN
 */

package com.tritcorp.transport.osmBusExtractor

import com.tritcorp.transport.configuration.Conf._
import com.tritcorp.transport.configuration.Conf.ss.implicits._
import com.tritcorp.transport.osmBusExtractor.InterPoints.InterPoint
import com.tritcorp.transport.osmBusExtractor.Views._
import com.typesafe.scalalogging.LazyLogging
import org.apache.spark.sql.Row
import org.apache.spark.sql.functions._
import org.apache.spark.sql.types.{StringType, StructField, StructType}
import slick.jdbc.PostgresProfile.api._

import scala.collection.mutable
import scala.collection.mutable.{ArrayBuffer, ListBuffer}
import scala.collection.parallel.ForkJoinTaskSupport
import scala.concurrent.Await
import scala.concurrent.duration.{Duration, SECONDS}

object Extraction extends LazyLogging {

  val stops_table = TableQuery[BusStops]

  /** The Inter-points table is created and populated but it is not used in the current version of this tool */
  val ips_table = TableQuery[InterPoints]

  case class TripMetaData(name: String = "N/A", colour: String = "N/A", operator: String = "N/A", ref: String = "N/A", from: String = "N/A", to: String = "N/A")

  /** Schema of the output csv s */
  private lazy val dataOutputSchema = List("way_id", "shape", "index", "name", "nb_lanes", "legal_speed", "speed_zone", "oneway", "shares_bike", "bike_box", "bus_lane", "dedicated_bus_lane",
    "length", "nb_traffic_signals", "nb_crossings", "nb_stops", "nb_give_ways", "nb_lvl_crossings", "roundabout", "stop_id_orig", "node_id_orig",
    "coord_x_orig", "coord_y_orig", "stop_id_dest", "node_id_dest", "coord_x_dest", "coord_y_dest", "nb_neighbouring_ways_bus_stop", "order_orig", "order_dest", "id_line",
    "line_name", "colour", "operator", "ref", "from", "to")
    .map(StructField(_, StringType, nullable = true))

  val linesDone: ListBuffer[TripMetaData] = new ListBuffer[TripMetaData]
  var linesCount: Int = 0

  /**
   * Returns the way from which the path finding should start depending on given bus stop, trip and the last way that was traveled through
   *
   * @param stop_id     The identifier of the bus stop to start from
   * @param last_way    The last way that was traveled trough (on the last iteration)
   * @param trip_osm_id The identifier of the bus trip
   * @return The identifier of the way to start the path finding from depending on if the bus stop nearest way is last_way or another one.
   */
  private def get_start_way(stop_id: String, last_way: Long, trip_osm_id: Long): Long = {

    val query = sql"""select way_id from way_node_#$trip_osm_id where node_id='#$stop_id' and way_id !='#$last_way'""".as[Long]
    val res = Await.result(db.run(query), Duration(db_query_timeout, SECONDS))

    if (res.isEmpty) {
      val query1 = sql"""select way_id from way_node2_#$trip_osm_id where node_id='#$stop_id' limit 1""".as[Long]
      val res1 = Await.result(db.run(query1), Duration(db_query_timeout, SECONDS))
      if (res1.isEmpty) {
        val query2 = sql"""select way_id from stops_ways_#$trip_osm_id where node_id='#$stop_id' and way_id ='#$last_way' limit 1""".as[Long]
        val res2 = Await.result(db.run(query2), Duration(db_query_timeout, SECONDS))
        if (res2.nonEmpty) res2(0) else -1L
      }
      else res1(0)
    }
    else
      res(0)
  }

  /**
   * Returns the way at which point the path finding should stop depending on the destination bus stop
   *
   * @param stop_id     The identifier of the bus stop to reach
   * @param trip_osm_id The identifier of the bus trip to whom stop_id belongs
   * @return The way at which point the path finding should stop depending on the destination bus stop
   */
  private def get_dest_way(stop_id: String, trip_osm_id: Long): Long = {
    val query = sql"""select way_id from way_node_#$trip_osm_id where node_id='#$stop_id'""".as[Long]
    val res = Await.result(db.run(query), Duration(db_query_timeout, SECONDS))

    if (res.nonEmpty) res(0)
    else -1L
  }

  /**
   * Returns the neighbouring ways of a given way within a bus trip
   *
   * @param way_id      The identifier of the way from which we identify its neighbours
   * @param trip_osm_id The identifier of the bus trip whom way_id belongs to
   * @return The way_id neighbours's identifiers as a Sequence of (id,name)
   */
  private def get_neighbours(way_id: Long, trip_osm_id: Long): Seq[(Long, String)] = {
    val query =
      sql"""select w1.osm_id, w1.tags->'name' as name from ways_#$trip_osm_id w0, ways_#$trip_osm_id w1 where w0.osm_id='#$way_id'
           and st_touches(w0.way,w1.way)""".as[(Long, String)]
    val res = Await.result(db.run(query), Duration(db_query_timeout, SECONDS))
    res
  }

  /**
   * GREEDY Path finding between two bus stops.
   * A THREAD IS LAUNCHED FOR EACH NEIGHBOURING WAY OF EACH WAY (using parallel collections)
   *
   * It returns a Set of possible paths.
   *
   * @param trip_osm_id               OSM id of the bus trip
   * @param current_way               OSM id of the current way (starting way at the original call)
   * @param order                     order of the current way within the path being processed
   * @param ways_set                  A set of ways OSM id used to control the end of the path finding
   * @param ordered_set               A set based on ways_set containing meta data about the orders of each way in the path
   * @param dest_way                  the way to reach from current_way
   * @param possible_paths            A set of possible found paths
   * @param possible_paths_size_limit Limit the number of paths to find. Default is 3
   */
  private def findPath(trip_osm_id: Long, current_way: Long, order: Int, ways_set: mutable.Set[Long], ordered_set: mutable.Set[(Int, Long)],
                       dest_way: Long, possible_paths: mutable.Set[List[(Int, Long)]], possible_paths_size_limit: Int = 3): Unit = {

    if (possible_paths.size < possible_paths_size_limit) {
      /** CASE END OF PATH */
      if (current_way == dest_way && ways_set.add(current_way)) {
        ordered_set += ((order + 1, current_way))
        possible_paths += ordered_set.toList.sorted
      }

      /** CASE END NOT REACHED YET
       * This implies that the way we are going to process has not been processed already
       * If not, we get all of its neighbours and recursively call findPath over them */
      else if (!ways_set.contains(current_way)) {
        val neighbours = get_neighbours(current_way, trip_osm_id).par
        neighbours.tasksupport = new ForkJoinTaskSupport(new scala.concurrent.forkjoin.ForkJoinPool(parallelism_lvl))
        neighbours.foreach {
          n =>
            ways_set += current_way
            ordered_set += ((order + 1, current_way))
            if (!ways_set.contains(n._1)) {
              val ways_set_aux = ways_set.clone()
              val ord_set_aux = ordered_set.clone()
              findPath(trip_osm_id, n._1, order + 1, ways_set_aux, ord_set_aux, dest_way, possible_paths)
            }
        }
      }
    }
  }

  /**
   * Extract the smallest path from a given set of paths
   *
   * @param paths the Set of paths to extract the smallest path from
   * @return the minimum path of paths
   */
  private def get_min_path(paths: mutable.Set[List[(Int, Long)]]): List[(Int, Long)] = {
    val minPaths: List[(Int, Long)] => Int = (path) => path.size
    if (paths.nonEmpty)
      paths.minBy(minPaths)
    else
      throw new Exception("""EMPTY PATHS ¯\_(ツ)_/¯""")
  }

  private def get_neighbours_way_per_stop(stop_id: String, trip_osm_id: Long): Int = {
    val query = sql"""select nb_neigh_ways from neighbours_per_stop_#$trip_osm_id where node_id='#$stop_id'""".as[Int]
    val res = Await.result(db.run(query), Duration(db_query_timeout, SECONDS))(0)
    res

  }

  /**
   * Extract data for a given way identified by way from the ways_data_#$trip_osm_id view.
   *
   * @param way          OSM id of the way
   * @param trip_osm_id  OSM id of the bus trip
   * @param orig         OSM ID of the origin bus stop of the way
   * @param dest         OSM ID of the destination bus stop of the way
   * @param split_length set to true to divide the way's length by 2
   * @return a Sequence containing the way's metadata
   */
  private def processWays(way: Long, index: Int, trip_osm_id: Long, orig: String, dest: String, split_length: Boolean = false): Seq[Any] = {
    //Used to divide way length by 2 when needed
    val split = if (split_length) "/2" else ""

    val query =
      sql"""select way_id, way, name, nb_lanes, legal_speed, speed_zone, oneway, bike, bike_box, bus_lane, dedicated_bus_lane, length#$split,
          nb_traffic_signals, nb_crossings, nb_stops, nb_give_ways, nb_lvl_crossings, roundabout
          from ways_data_#$trip_osm_id where way_id='#$way'""".as[(String, String, String, String, String, String, String, String, String, String, String, String, String, String, String, String, String, String)]

    val res = Await.result(db.run(query), Duration(db_query_timeout, SECONDS))(0)


    //This query and the following one gather geographical data about bus stops
    val query2 = sql"""select stop_id, node_id, st_x(node_way) as coord_x_orig, st_y(node_way) as coord_y_orig from way_node_#$trip_osm_id where node_id='#$orig'""".as[(String, String, String, String)]
    val res2 = Await.result(db.run(query2), Duration(db_query_timeout, SECONDS))(0)

    val query3 = sql"""select stop_id, node_id, st_x(node_way) as coord_x_dest, st_y(node_way) as coord_y_dest from way_node_#$trip_osm_id  where node_id='#$dest'""".as[(String, String, String, String)]
    val res3 = Await.result(db.run(query3), Duration(db_query_timeout, SECONDS))(0)

    val ret = Seq(res._1, res._2, index.toString, res._3, res._4, res._5, res._6,
      res._7, res._8, res._9, res._10, res._11, res._12, res._13,
      res._14, res._15, res._16, res._17, res._18, res2._1, res2._2,
      res2._3, res2._4, res3._1, res3._2, res3._3, res3._4)

    ret
  }


  private def work_on_trip(trip_osm_id: Long) = {
    val trip_ips = update_temp_views(trip_osm_id)

    if (trip_ips.nonEmpty) {

      val meta_query =
        sql"""select tags->'name' as name, tags->'colour' as colour, tags->'operator' as operator, tags->'ref' as ref, tags->'from' as from, tags->'to' as to
          from relations where id=#$trip_osm_id""".as[(String, String, String, String, String, String)]

      val metadata = Await.result(db.run(meta_query), Duration(db_query_timeout, SECONDS)).map { md =>
        TripMetaData(md._1, md._2, md._3, md._4, md._5, md._6)
      }.head

      logger.warn(s"WORKING ON LINE ${metadata.name} with osm id $trip_osm_id")
      var last_way = 0L
      var terminalStart = true
      val current_ips = trip_ips.sortBy(_.order1).par
      val ordreEnd = current_ips.last.order2

      var ipCount = 1
      lazy val dataOutput: ListBuffer[Row] = new ListBuffer[Row]()

      current_ips.tasksupport = new ForkJoinTaskSupport(new scala.concurrent.forkjoin.ForkJoinPool(parallelism_lvl))
      current_ips.foreach {
        ip =>
          ipCount += 1
          val start_way = get_start_way(ip.orig, last_way, trip_osm_id)
          val dest_way = get_dest_way(ip.dest, trip_osm_id)
          if ((start_way == -1) || (dest_way == -1)) {
            logger.info(s"TRIP $trip_osm_id : INTER_POINTS UNREACHABLE : ${ip.orig} -> ${ip.dest}")
          }
          else {
            val ways_set: mutable.Set[Long] = mutable.Set()
            val ordered_ways_set: mutable.Set[(Int, Long)] = mutable.Set()
            val possiblePaths: mutable.Set[List[(Int, Long)]] = mutable.Set()
            if (start_way == dest_way) {
              possiblePaths += List((1, start_way))
              last_way = dest_way
            }
            else {

              logger.info(s"start_way : $start_way, dest_way : $dest_way")
              findPath(trip_osm_id, start_way, 0, ways_set, ordered_ways_set, dest_way, possiblePaths)
              logger.info(s"TIME, IP : ${ip.orig}, ${ip.dest}")
            }
            try {
              val path = get_min_path(possiblePaths).par
              last_way = dest_way
              path.tasksupport = new ForkJoinTaskSupport(new scala.concurrent.forkjoin.ForkJoinPool(parallelism_lvl))
              path.foreach {
                way =>

                  /**
                   * Getting way's data with length divided by 2 (arbitrarily) when the way is is ending an inter-point and starting the next one and
                   * not bound to a terminal
                   * - If we are at the starting terminal, hence the number of neighbouring ways must be 1
                   * - else for each of the bus stops that compose the interpoint, we process the length of the neighbouring ways depending on how many
                   * of them there are
                   * */
                  val way_data =
                    if (terminalStart) {
                      terminalStart = false
                      processWays(way._2, way._1, trip_osm_id, orig = ip.orig, dest = ip.dest) ++ Seq("1")
                    } ++ Seq(ip.order1.toString, ip.order2.toString, trip_osm_id.toString, metadata.name, metadata.colour,
                      metadata.operator, metadata.ref, metadata.from, metadata.to)
                    else {
                      val neigh_orig = get_neighbours_way_per_stop(ip.orig, trip_osm_id)
                      val neigh_dest = get_neighbours_way_per_stop(ip.orig, trip_osm_id)
                      if (way.equals(path.head)) {
                        processWays(way._2, way._1, trip_osm_id, split_length = (neigh_orig == 1), orig = ip.orig, dest = ip.dest) ++ Seq(neigh_orig.toString)
                      } else if (way.equals(path.last)) {
                        if (ip.order2 < ordreEnd) {
                          processWays(way._2, way._1, trip_osm_id, split_length = (neigh_dest == 1), orig = ip.orig, dest = ip.dest) ++ Seq(neigh_dest.toString)
                        }

                        /** case destination terminal */
                        else {
                          processWays(way._2, way._1, trip_osm_id, orig = ip.orig, dest = ip.dest) ++ Seq("1")
                        }
                      } else
                        processWays(way._2, way._1, trip_osm_id, orig = ip.orig, dest = ip.dest) ++ Seq("0")
                    } ++ Seq(ip.order1.toString, ip.order2.toString, trip_osm_id.toString, metadata.name, metadata.colour,
                      metadata.operator, metadata.ref, metadata.from, metadata.to)

                  dataOutput.append(Row.fromSeq(way_data))
                  logger.info(s"size : +${dataOutput.size}")
                  logger.info(s"$way_data")

              }
            } catch {
              case e: Exception => {
                logger.warn(s"ERROR WHILE FETCHING PATHS FOR INTER-POINT COMPOSED OF ${ip.orig} -> ${ip.dest} OF TRIP WHICH OSM ID IS $trip_osm_id\n MESSAGE : ${e.getMessage}")
                //e.printStackTrace()
              }
            }
          }

          logger.info(s"TRIP ${trip_osm_id} : INTER-POINT DONE : ${ip.orig} -> ${ip.dest}")
      } //END LINE
      logger.info(s"end_line, ${dataOutput.size}")
      val df = ss.createDataFrame(sc.parallelize(dataOutput), StructType(dataOutputSchema)).cache
      if (df.count > 0) {

        val res = df
          .na.fill("1", Seq("nb_lanes"))
          .na.fill("0", Seq("nb_traffic_signals", "nb_crossings", "nb_stops", "nb_give_ways", "nb_lvl_crossings"))
          .na.fill(default_max_speed, Seq("legal_speed"))
          .withColumn("speed_zone", when($"speed_zone" === "FR:30" or $"speed_zone" === "30", "true").otherwise("false"))
          .withColumn("oneway", when($"oneway" === "yes", "true").otherwise("false"))
          .withColumn("roundabout", when($"roundabout" === "1", "true").otherwise("false"))
          .selectExpr("cast(way_id as long) as way_id", "shape", "cast(index as integer) as index", "name", "cast(nb_lanes as integer) as nb_lanes",
            "cast(legal_speed as integer) as legal_speed", "cast(speed_zone as boolean) as speed_zone_30", "cast(oneway as boolean) as oneway", "cast(shares_bike as boolean) as shares_bike",
            "cast(bike_box as boolean) as bike_box", "cast(bus_lane as boolean) as bus_lane", "cast(dedicated_bus_lane as boolean) as dedicated_bus_lane", "cast(length as double) as length",
            "cast(nb_traffic_signals as integer) as nb_traffic_signals", "cast(nb_crossings as integer) as nb_crossings", "cast(nb_stops as integer) as nb_stops",
            "cast(nb_give_ways as integer) as nb_give_ways", "cast(nb_lvl_crossings as integer) as nb_lvl_crossings", "cast(roundabout as boolean) as roundabout",
            "stop_id_orig", "cast(node_id_orig as long) as node_id_orig", "cast(coord_x_orig as double) as coord_x_orig",
            "cast(coord_y_orig as double) as coord_y_orig", "stop_id_dest", "node_id_dest", "coord_x_dest", "cast(coord_y_dest as double) as coord_y_dest",
            "cast(nb_neighbouring_ways_bus_stop as integer) as nb_neighbouring_ways_bus_stop", "cast(order_orig as integer) as order_orig", "cast(order_dest as integer) as order_dest", "id_line",
            "line_name", "colour", "operator", "ref", "from", "to")
          .orderBy("id_line", "order_orig", "order_dest", "index")
          .repartition(1)

        res.cache
        res.show

        res
          .write
          .option("delimiter", ";")
          .option("header", "true")
          .mode("Append")
          .csv(appConf.getString("app.data.osm_module.output") +
            s"/key=${Option(metadata.ref).getOrElse(trip_osm_id.toString).replace("""/""", "_").replace("""\""", "_")}-" +
            s"${Option(metadata.from).getOrElse(current_ips.head.orig).replace("""/""", "_").replace("""\""", "_")}-" +
            s"${Option(metadata.to).getOrElse(current_ips.last.dest).replace("""/""", "_").replace("""\""", "_")}")

        res.unpersist()
      }
      df.unpersist()
      linesDone.append(metadata)
      logger.warn(s"FINISHED : ${metadata.name} with osm id $trip_osm_id")
      logger.warn(s"TOTAL DONE : ${linesDone.length}/$linesCount")

    }
    else {
      logger.warn(s"NO DATA FOR TRIP WITH OSM ID $trip_osm_id")
      linesDone.append(TripMetaData())
      logger.warn(s"TOTAL DONE : ${linesDone.length}/$linesCount")
    }
  }


  def main(args: Array[String]): Unit = {

    /** Clear the DB before using it again */

    clear_all_temp_views()
    val dropAll: DBIO[Int] = sqlu"""drop table if exists interpoints, bus_stops cascade"""
    val setup = DBIO.seq(stops_table.schema.createIfNotExists, ips_table.schema.createIfNotExists)

    val relations: DBIO[Int] =
      sqlu"""create or replace view relations as
            (select id, way_off, rel_off, parts, hstore(members) members, hstore(tags) tags from planet_osm_rels)"""
    Await.result(db.run(dropAll >> setup >> relations), Duration(db_query_timeout, SECONDS))

    val relations_query = network_or_operator match {
      case 0 => {
        sql"""select id from relations where tags->'route'='#$rel_type' and tags->'network' in ('#${network_names.mkString("','")}')""".as[Long]
      }
      case 1 => {
        sql"""select id from relations where tags->'route'='#$rel_type' and tags->'operator' in ('#${operator_names.mkString("','")}')""".as[Long]
      }
      case 2 => {
        sql"""select id from relations where tags->'route'='#$rel_type' and tags->'operator' in ('#${operator_names.mkString("','")}') and tags->'network'in ('#${network_names.mkString("','")}')""".as[Long]
      }
      case _ => {
        throw new Exception("network_or_operator should be 0,1 or 2 (cf Application.conf)")
      }
    }

    if (parallelism_for_trips) {
      val rels = Await.result(db.run(relations_query), Duration(db_query_timeout, SECONDS)).par
      linesCount = rels.length

      rels.tasksupport = new ForkJoinTaskSupport(new scala.concurrent.forkjoin.ForkJoinPool(parallelism_lvl))
      rels.foreach { rel =>
        work_on_trip(rel)
        clear_temp_views(rel)
      }
    }
    else {
      val rels = Await.result(db.run(relations_query), Duration(db_query_timeout, SECONDS))
      linesCount = rels.length
      rels.foreach { rel =>
        work_on_trip(rel)
        clear_temp_views(rel)
      }
    }
    clear_all_temp_views()
    if (clear_db_on_exit) {
      Await.result(db.run(dropAll >> setup >> relations), Duration(db_query_timeout, SECONDS))
    }
  }
}