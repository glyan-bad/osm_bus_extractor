/*
 * Copyright (c) 2020.  Gauthier LYAN
 */

package com.tritcorp.transport.configuration

import com.typesafe.config.{Config, ConfigFactory}
import org.apache.spark.serializer.KryoSerializer
import org.apache.spark.{SparkConf, SparkContext}
import org.apache.spark.sql.{SQLContext, SparkSession}
import slick.jdbc.JdbcBackend._

object Conf {
  lazy val appConf: Config = ConfigFactory.load()

  lazy val master: String = appConf.getString("app.sparkConf.master")
  lazy val par: String = appConf.getString("app.sparkConf.par")
  lazy val shuffle: String = appConf.getString("app.sparkConf.shuffle")
  lazy val coreNum: String = appConf.getString("app.sparkConf.coreNum")

  //driver conf
  lazy val driverMem: String = appConf.getString("app.sparkConf.driver.memory")
  lazy val driverCores: String = appConf.getString("app.sparkConf.driver.cores")

  //workers conf
  lazy val workersMem: String = appConf.getString("app.sparkConf.workers.memory")
  lazy val workersCores: String = appConf.getString("app.sparkConf.workers.cores")

  //executors conf
  lazy val executorsMem: String = appConf.getString("app.sparkConf.executors.memory")
  lazy val executorsCores: String = appConf.getString("app.sparkConf.executors.cores")

  lazy val conf: SparkConf = new SparkConf().setAppName("osm_bus_extractor")

  //set master
  if (master.equalsIgnoreCase("local"))
    conf.set("spark.master", master + "[" + coreNum + "]")
  else
    conf.set("spark.master", master)

  //set global stuff
  conf.set("spark.default.parallelism", par)
  conf.set("spark.sql.shuffle.partitions", shuffle)
  conf.set("spark.scheduler.listenerbus.eventqueue.capacity", "1000000")
  conf.set("spark.serializer", "org.apache.spark.serializer.KryoSerializer")
  conf.set("spark.kryo.unsafe", "true")

  //set driver
  conf.set("spark.driver.memory", driverMem)
  conf.set("spark.driver.cores", driverCores)
  conf.set("spark.driver.extraJavaOptions", "-XX:+UseG1G")

  //set workers
  conf.set("spark.worker.memory", workersMem)
  conf.set("spark.worker.cores", workersCores)

  //set executors
  conf.set("spark.executor.memory", executorsMem)
  conf.set("spark.executor.cores", executorsCores)
  conf.set("spark.executor.extraJavaOptions", "-XX:+UseG1G")

  lazy val ss: SparkSession = SparkSession.builder().config(conf).getOrCreate()
  lazy val sc: SparkContext = ss.sparkContext
  lazy val sqlContext: SQLContext = ss.sqlContext
  sc.setLogLevel("WARN")


  lazy val db = Database.forConfig("app.postgres")

  lazy val rel_type = "bus"
  lazy val network_names: Array[String] = appConf.getList("app.data.osm_module.network_names").unwrapped().toArray().map {
    _.toString
  }
  lazy val operator_names: Array[String] = appConf.getList("app.data.osm_module.operator_names").unwrapped().toArray().map {
    _.toString
  }

  lazy val physicalBusStop:Boolean = appConf.getBoolean("app.data.osm_module.prefer_physical_bus_stops")

  lazy val ref_key_stops: String = {
    val k = appConf.getString("app.data.osm_module.ref_key_stops")
    if (k.isEmpty) "name" else k
  }

  lazy val default_max_speed: String = appConf.getInt("app.data.osm_module.default_max_speed").toString
  lazy val use_ref_key_stops: Boolean = appConf.getBoolean("app.data.osm_module.use_ref_key_stops")
  lazy val network_or_operator: Int = appConf.getInt("app.data.osm_module.network_or_operator")
  lazy val ways_shape_format: String = appConf.getString("app.data.osm_module.ways_shape_format")
  lazy val nearestRadiusMeter = 20
  lazy val epsg: Int = appConf.getInt("app.data.osm_module.epsg")
  lazy val parallelism_lvl: Int = appConf.getInt("app.data.osm_module.parallelism")
  lazy val parallelism_for_trips: Boolean = appConf.getBoolean("app.data.osm_module.parallelism_for_trips")
  lazy val clear_db_on_exit: Boolean = appConf.getBoolean("app.data.osm_module.clear_db_on_exit")
  lazy val db_query_timeout: Int = appConf.getInt("app.postgres.query_timeout_seconds")

  db.createSession()
}
